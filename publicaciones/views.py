from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime , timedelta
from publicaciones.models import Publicaciones
from publicaciones.models import Seguidos_seguidor
# Create your views here.

class publicaciones():
    def inicio(request):
        request.session['usuario'] = "juane"
        seguidos_porusuario = Seguidos_seguidor.objects.filter(userName_seguidor__icontains = request.session['usuario'])  #busca los seguidores del usuario logueado
        usuarios_publicaciones = [request.session['usuario']]
        for seguidos in seguidos_porusuario:        #agrega a la lista los seguidos 
            usuarios_publicaciones.append(seguidos.seguido)
        publicaciones= Publicaciones.objects.filter(autor__in = usuarios_publicaciones).order_by('-fecha', '-hora') #busca las publicaciones de los seguidores y propias
        return render(request,"feed.html",{"publicaciones":publicaciones,"usuario":request.session['usuario']})
    def buscar(request):
        if(request.GET["texto_buscado"]):
            requestbusqueda = request.GET["texto_buscado"]                        
            if(requestbusqueda[0] == "@"):
                texto_buscado=Seguidos_seguidor.objects.filter(userName_seguidor__icontains=requestbusqueda[1:])
                usuario_ok = 1
                return render(request,"buscador.html",{"buscado":texto_buscado,"query":requestbusqueda,"usuario_ok":usuario_ok})
            else:
                texto_buscado = Publicaciones.objects.filter(texto__icontains=requestbusqueda) #busca en la base de datos todas las publicaciones que coinciden con lo escrito
                usuario_ok = 0
                return render(request,"buscador.html",{"buscado":texto_buscado,"query":requestbusqueda,"usuario_ok":usuario_ok})
        else: #si no hay nada entonces sale error
            mensaje = "Error request"
            return HttpResponse(mensaje)
    def republicar(request):        
        if(request.GET["republicar"]):
            requestrepublicar = request.GET["republicar"]
            republicarautor = request.GET["autor"] #se recibe quien es el autor de la publicacion original
            republicartexto = request.GET["texto_republicado"] #se recibe el texto original de la publicacion
            fecha_hoy = datetime.now() - timedelta(hours=3)
            fecha_now = fecha_hoy.strftime("%Y-%m-%d") 
            hora_now = fecha_hoy.strftime("%H:%M") 
            Publicaciones.objects.create(texto = republicartexto , autor = request.session['usuario'], fecha = fecha_now,hora=hora_now,republicacion_autor_original = republicarautor ,republicacion = True)  #se crea la republicacion pero a diferencia de la publicacion se da como true el booleano que nos dice que es republicacion y se escribe al autor original
            return render(request,"republicar.html")
    def modificar(request):
        if(request.GET["modificar"]):
            modificar_id = request.GET["id_modificar"]
            texto_modificable = Publicaciones.objects.filter(id = modificar_id) #se trae el texto a modificar con el id para evitar equivocaciones 
            return render(request,"modificar.html",{"texto_modificable":texto_modificable })
    def modificar_efectivo(request):
        if(request.GET["modificar_efectivo"]):
            modificar_id = request.GET["id_modificacion"] #el id de la publicacion a modificar
            texto_mod = request.GET["texto"] #el nuevo texto que va a sobreescribir el antiguo
            Publicaciones.objects.filter(id = modificar_id).update(texto = texto_mod , modificacion = True) #se busca y actualiza el texto y ademas se da como True el booleano de modificacion
            return render(request,"modificar_efectivo.html")
    def eliminar(request):
        if(request.GET["eliminar"]):
            eliminar_id = request.GET["id_eliminar"] #se recibe el id unicamente
            Publicaciones.objects.filter(id = eliminar_id).delete() #se elimina solo la publicacion con el id requerido
            return render(request,"eliminar.html")


