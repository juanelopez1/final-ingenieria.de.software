from django.db import models

# Create your models here.

class Publicaciones(models.Model):
    texto= models.CharField(max_length=140)
    autor = models.CharField(max_length = 20) #el que requiere la modificacion o creacion
    fecha= models.DateField()
    hora = models.TimeField()
    republicacion = models.BooleanField(null = True) # es True solo cuando se ha hecho una republicacion por eso empieza en null
    modificacion = models.BooleanField(null = True) # es True solo cuando se ha hecho una modificacion
    republicacion_autor_original = models.CharField(max_length = 20,blank=True) #el autor que se republica
    
class Seguidos_seguidor(models.Model):
    userName_seguidor = models.CharField(max_length=20)
    seguido = models.CharField(max_length=20)
    fecha = models.DateField()
    hora = models.TimeField()

